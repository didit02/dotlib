import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DashboardComponent } from "./modules/dashboard/dashboard.component";
import { PagenotfoundComponent } from "./modules/pagenotfound/pages/pagenotfound.component";
// import { AuthGuard } from "./core/guard/auth.guard";
const routes: Routes = [
  {
    path: "",
    component: DashboardComponent,
    // canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: "login",
        pathMatch: "full"
      },
      {
        path: "home",
        loadChildren: "./modules/home/home.module#HomeModule"
      },
    ]
  },
  {
    path: "login",
    loadChildren: "./modules/login/login.module#LoginModule"
  },
  {
    path: "forgotpassword",
    loadChildren:
      "./modules/forgotpassword/forgotpassword.module#ForgotpasswordModule"
  },
  {
    path: "**",
    redirectTo: "/404"
  },
  {
    path: "404",
    component: PagenotfoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
