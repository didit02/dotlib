const { exec } = require("child_process");
var fs = require('fs');
var fscop = require("fs-extra");
const readline = require("readline");
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
exec("node -v", (error, stdout, stderr) => {
    if (error) {
        console.log(`error: ${error.message}`);
        return;
    } else if (stderr) {
        console.log(`stderr: ${stderr}`);
        return;
    } else {
        exec("npm -v", (error, stdout, stderr) => {
            if (error) {
                console.log(`error: ${error.message}`);
                return;
            } else if (stderr) {
                console.log(`stderr: ${stderr}`);
                return;
            } else {
                exec("ng --version", (error, stdout, stderr) => {
                    if (error) {
                        console.log(`error: ${error.message}`);
                        return;
                    } else if (stderr) {
                        console.log(`stderr: ${stderr}`);
                        return;
                    } else {
                        rl.question("What is your project name ? ", function (name) {
                            rl.close();
                            if (!fs.existsSync(name)) {
                                var child = require('child_process').exec('ng new ' + name + ' --routing=true --style=scss')
                                child.stdout.pipe(process.stdout)
                                child.on('exit', function (code, signal) {
                                    fs.mkdirSync(name + '/src/app/core');
                                    fs.mkdirSync(name + '/src/app/config');
                                    fs.mkdirSync(name + '/src/app/modules');
                                    fs.mkdirSync(name + '/src/app/shared');
                                    fs.mkdirSync(name + '/src/app/core/authentication');
                                    fs.mkdirSync(name + '/src/app/core/guard');
                                    fs.mkdirSync(name + '/src/app/core/http');
                                    fs.mkdirSync(name + '/src/app/core/interceptor');
                                    fs.mkdirSync(name + '/src/app/core/mock');
                                    fs.mkdirSync(name + '/src/app/core/redux');
                                    fs.mkdirSync(name + '/src/app/core/services');
                                    fs.mkdirSync(name + '/src/app/shared/components');
                                    fs.mkdirSync(name + '/src/app/shared/directives');
                                    fs.mkdirSync(name + '/src/app/shared/models');
                                    fs.mkdirSync(name + '/src/app/shared/pipes');
                                    fs.mkdirSync(name + '/src/app/modules/login');
                                    fs.mkdirSync(name + '/src/app/modules/login/pages');
                                    fs.mkdirSync(name + '/src/app/modules/register');
                                    fs.mkdirSync(name + '/src/app/modules/register/pages');
                                    fs.mkdirSync(name + '/src/app/modules/pagenotfound');
                                    fs.mkdirSync(name + '/src/app/modules/pagenotfound/pages');
                                    fs.mkdirSync(name + '/src/app/modules/forgotpassword');
                                    fs.mkdirSync(name + '/src/app/modules/forgotpassword/pages');
                                    fs.mkdirSync(name + '/src/app/modules/changepassword');
                                    fs.mkdirSync(name + '/src/app/modules/changepassword/pages');
                                    fs.mkdirSync(name + '/src/app/modules/home');
                                    fs.mkdirSync(name + '/src/app/modules/dashboard');
                                    fs.mkdirSync(name + '/src/app/core/footer');
                                    fs.mkdirSync(name + '/src/app/core/header');
                                    fs.mkdirSync(name + '/src/app/core/sidebar');
                                    fs.mkdirSync(name + '/src/app/modules/home/pages');
                                    fs.mkdirSync(name + '/src/app/core/services/statemanagement');
                                    fs.mkdirSync(name + '/src/app/core/services/loader');
                                    fs.mkdirSync(name + '/src/app/shared/components/alerts');
                                    fs.mkdirSync(name + '/src/app/shared/components/alerts/pages');
                                    fscop.copy('component/template2', name + '/src/assets/template2');
                                    fscop.copy('component/jquery', name + '/node_modules/jquery');
                                    fscop.copy('component/bootstrap', name + '/node_modules/bootstrap');
                                    fs.createReadStream('component/app.component.html').pipe(fs.createWriteStream(name + '/src/app/app.component.html'));
                                    fs.closeSync(fs.openSync(name + '/src/app/core/guard/auth.guard.ts', 'a'))
                                    fs.createReadStream('component/app-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/app-routing.module.ts'));
                                    fs.createReadStream('component/app.module.ts').pipe(fs.createWriteStream(name + '/src/app/app.module.ts'));
                                    fs.createReadStream('component/dashboard.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/dashboard/dashboard.component.html'));
                                    fs.createReadStream('component/dashboard.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/dashboard/dashboard.component.ts'));
                                    fs.createReadStream('component/dashboard.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/dashboard/dashboard.component.scss'));
                                    fs.createReadStream('component/dashboard.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/modules/dashboard/dashboard.component.spec.ts'));
                                    fs.createReadStream('component/home.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/home/pages/home.component.html'));
                                    fs.createReadStream('component/home.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/home/pages/home.component.ts'));
                                    fs.createReadStream('component/home.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/home/pages/home.component.scss'));
                                    fs.createReadStream('component/home-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/home/home-routing.module.ts'));
                                    fs.createReadStream('component/home.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/home/home.module.ts'));
                                    fs.createReadStream('component/footer.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/core/footer/footer.component.spec.ts'));
                                    fs.createReadStream('component/footer.component.html').pipe(fs.createWriteStream(name + '/src/app/core/footer/footer.component.html'));
                                    fs.createReadStream('component/footer.component.ts').pipe(fs.createWriteStream(name + '/src/app/core/footer/footer.component.ts'));
                                    fs.createReadStream('component/footer.component.scss').pipe(fs.createWriteStream(name + '/src/app/core/footer/footer.component.scss'));
                                    fs.createReadStream('component/header.component.html').pipe(fs.createWriteStream(name + '/src/app/core/header/header.component.html'));
                                    fs.createReadStream('component/header.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/core/header/header.component.spec.ts'));
                                    fs.createReadStream('component/header.component.ts').pipe(fs.createWriteStream(name + '/src/app/core/header/header.component.ts'));
                                    fs.createReadStream('component/header.component.scss').pipe(fs.createWriteStream(name + '/src/app/core/header/header.component.scss'));
                                    fs.createReadStream('component/sidebar.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/core/sidebar/sidebar.component.spec.ts'));
                                    fs.createReadStream('component/sidebar.component.html').pipe(fs.createWriteStream(name + '/src/app/core/sidebar/sidebar.component.html'));
                                    fs.createReadStream('component/sidebar.component.ts').pipe(fs.createWriteStream(name + '/src/app/core/sidebar/sidebar.component.ts'));
                                    fs.createReadStream('component/sidebar.component.scss').pipe(fs.createWriteStream(name + '/src/app/core/sidebar/sidebar.component.scss'));
                                    fs.createReadStream('component/loader.service.ts').pipe(fs.createWriteStream(name + '/src/app/core/services/loader/loader.service.ts'));
                                    fs.createReadStream('component/statemanagement.service.ts').pipe(fs.createWriteStream(name + '/src/app/core/services/statemanagement/statemanagement.service.ts'));
                                    fs.createReadStream('component/shared.module.ts').pipe(fs.createWriteStream(name + '/src/app/shared/shared.module.ts'));
                                    fs.createReadStream('component/alerts.component.html').pipe(fs.createWriteStream(name + '/src/app/shared/components/alerts/pages/alerts.component.html'));
                                    fs.createReadStream('component/alerts.component.ts').pipe(fs.createWriteStream(name + '/src/app/shared/components/alerts/pages/alerts.component.ts'));
                                    fs.createReadStream('component/alerts.component.scss').pipe(fs.createWriteStream(name + '/src/app/shared/components/alerts/pages/alerts.component.scss'));
                                    fs.createReadStream('component/alerts.module.ts').pipe(fs.createWriteStream(name + '/src/app/shared/components/alerts/alerts.module.ts'));
                                    fs.createReadStream('component/auth.guard.ts').pipe(fs.createWriteStream(name + '/src/app/core/guard/auth.guard.ts'));
                                    fs.createReadStream('component/login.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/login/pages/login.component.html'));
                                    fs.createReadStream('component/login.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/login/pages/login.component.ts'));
                                    fs.createReadStream('component/login.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/login/pages/login.component.scss'));
                                    fs.createReadStream('component/login-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/login/login-routing.module.ts'));
                                    fs.createReadStream('component/login.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/login/login.module.ts'));
                                    fs.createReadStream('component/login.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/modules/login/pages/login.component.spec.ts'));
                                    fs.createReadStream('component/pagenotfound.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pages/pagenotfound.component.html'));
                                    fs.createReadStream('component/pagenotfound.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pages/pagenotfound.component.ts'));
                                    fs.createReadStream('component/pagenotfound.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pages/pagenotfound.component.scss'));
                                    fs.createReadStream('component/pagenotfound-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pagenotfound-routing.module.ts'));
                                    fs.createReadStream('component/pagenotfound.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pagenotfound.module.ts'));
                                    fs.createReadStream('component/pagenotfound.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/modules/pagenotfound/pages/pagenotfound.component.spec.ts'));
                                    fs.createReadStream('component/changepassword.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/pages/changepassword.component.html'));
                                    fs.createReadStream('component/changepassword.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/pages/changepassword.component.ts'));
                                    fs.createReadStream('component/changepassword.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/pages/changepassword.component.scss'));
                                    fs.createReadStream('component/changepassword-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/changepassword-routing.module.ts'));
                                    fs.createReadStream('component/changepassword.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/changepassword.module.ts'));
                                    fs.createReadStream('component/changepassword.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/modules/changepassword/pages/changepassword.component.spec.ts'));
                                    fs.createReadStream('component/authentication.service.ts').pipe(fs.createWriteStream(name + '/src/app/core/authentication/authentication.service.ts'));
                                    fs.createReadStream('component/authentication.service.spec.ts').pipe(fs.createWriteStream(name + '/src/app/core/authentication/authentication.service.spec.ts'));
                                    fs.createReadStream('component/forgotpassword.component.html').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/pages/forgotpassword.component.html'));
                                    fs.createReadStream('component/forgotpassword.component.ts').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/pages/forgotpassword.component.ts'));
                                    fs.createReadStream('component/forgotpassword.component.scss').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/pages/forgotpassword.component.scss'));
                                    fs.createReadStream('component/forgotpassword-routing.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/forgotpassword-routing.module.ts'));
                                    fs.createReadStream('component/forgotpassword.module.ts').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/forgotpassword.module.ts'));
                                    fs.createReadStream('component/forgotpassword.component.spec.ts').pipe(fs.createWriteStream(name + '/src/app/modules/forgotpassword/pages/forgotpassword.component.spec.ts'));
                                    fs.createReadStream('component/styles.scss').pipe(fs.createWriteStream(name + '/src/styles.scss'));
                                    fs.writeFile(name + '/angular.json', `
{
    "$schema": "./node_modules/@angular/cli/lib/config/schema.json",
        "version": 1,
                                        "newProjectRoot": "projects",
                                        "projects": {
                                          "`+ name + `": {
                                            "projectType": "application",
                                            "schematics": {
                                              "@schematics/angular:component": {
                                                "style": "scss"
                                              }
                                            },
                                            "root": "",
                                            "sourceRoot": "src",
                                            "prefix": "app",
                                            "architect": {
                                              "build": {
                                                "builder": "@angular-devkit/build-angular:browser",
                                                "options": {
                                                  "outputPath": "dist/`+ name + `",
                                                  "index": "src/index.html",
                                                  "main": "src/main.ts",
                                                  "polyfills": "src/polyfills.ts",
                                                  "tsConfig": "tsconfig.app.json",
                                                  "assets": [
                                                    "src/favicon.ico",
                                                    "src/assets"
                                                  ],
                                                  "styles": [
                                                    "src/styles.scss",
                                                    "./node_modules/bootstrap/dist/css/bootstrap.min.css"
                                                  ],
                                                  "scripts": [
                                                    "./node_modules/jquery/dist/jquery.min.js"
                                                  ]
                                                },
                                                "configurations": {
                                                  "production": {
                                                    "fileReplacements": [
                                                      {
                                                        "replace": "src/environments/environment.ts",
                                                        "with": "src/environments/environment.prod.ts"
                                                      }
                                                    ],
                                                    "optimization": true,
                                                    "outputHashing": "all",
                                                    "sourceMap": false,
                                                    "extractCss": true,
                                                    "namedChunks": false,
                                                    "aot": true,
                                                    "extractLicenses": true,
                                                    "vendorChunk": false,
                                                    "buildOptimizer": true,
                                                    "budgets": [
                                                      {
                                                        "type": "initial",
                                                        "maximumWarning": "2mb",
                                                        "maximumError": "5mb"
                                                      }
                                                    ]
                                                  }
                                                }
                                              },
                                              "serve": {
                                                "builder": "@angular-devkit/build-angular:dev-server",
                                                "options": {
                                                  "browserTarget": "`+ name + `:build"
                                                },
                                                "configurations": {
                                                  "production": {
                                                    "browserTarget": "`+ name + `:build:production"
                                                  }
                                                }
                                              },
                                              "extract-i18n": {
                                                "builder": "@angular-devkit/build-angular:extract-i18n",
                                                "options": {
                                                  "browserTarget": "`+ name + `:build"
                                                }
                                              },
                                              "test": {
                                                "builder": "@angular-devkit/build-angular:karma",
                                                "options": {
                                                  "main": "src/test.ts",
                                                  "polyfills": "src/polyfills.ts",
                                                  "tsConfig": "tsconfig.spec.json",
                                                  "karmaConfig": "karma.conf.js",
                                                  "assets": [
                                                    "src/favicon.ico",
                                                    "src/assets"
                                                  ],
                                                  "styles": [
                                                    "src/styles.scss",
                                                    "./node_modules/bootstrap/dist/css/bootstrap.min.css"
                                                  ],
                                                  "scripts": [
                                                    "./node_modules/jquery/dist/jquery.min.js"
                                                  ]
                                                }
                                              },
                                              "lint": {
                                                "builder": "@angular-devkit/build-angular:tslint",
                                                "options": {
                                                  "tsConfig": [
                                                    "tsconfig.app.json",
                                                    "tsconfig.spec.json",
                                                    "e2e/tsconfig.json"
                                                  ],
                                                  "exclude": [
                                                    "**/node_modules/**"
                                                  ]
                                                }
                                              },
                                              "e2e": {
                                                "builder": "@angular-devkit/build-angular:protractor",
                                                "options": {
                                                  "protractorConfig": "e2e/protractor.conf.js",
                                                  "devServerTarget": "`+ name + `:serve"
                                                },
                                                "configurations": {
                                                  "production": {
                                                    "devServerTarget": "`+ name + `:serve:production"
                                                  }
                                                }
                                              }
                                            }
                                          }},
                                        "defaultProject": "`+ name + `"
                                      }`, function (err) {
                                        if (err) {
                                            return console.log(err);
                                        } else {
                                            fs.writeFile(name + '/src/index.html', `
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>`+ name + `</title>
  <base href="/">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/x-icon" href="favicon.ico">
  <link rel="stylesheet" href="assets/template2/css/app.css">
</head>
<body class="light mat-typography">
  <app-root></app-root>
</body>
</html>

                                `, function (err, data) {
                                                if (err) {
                                                    return console.log(err);
                                                }
                                            });
                                        }
                                    });
                                })

                            } else {
                                console.log('Folder exists');
                            }
                        });
                    }
                })
            }
        })
    }
})